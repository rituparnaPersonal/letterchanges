import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'LetterChanges';
  letter:any;
  NewForm:any;


  constructor(){
    console.log('Changes',this.letter)
  }


  LetterChanges(){
   
    var init:any= 'a'
    let letter_only = this.letter.toLowerCase().split("");
    let newLetter = [];
    for(let i = 0; i< letter_only.length; i++){
      if(this.letter.charCodeAt(i) < 97 || this.letter.charCodeAt(i) > 122){ // 97 is the ASCII value of 'a' and 122 is the ASCII value of 'z'
            newLetter.push(letter_only[i]);
          
          }else{
          
           var n = letter_only[i].charCodeAt() - init.charCodeAt();
           n = (n + 1) % 26; //Total alphabet 26;
           letter_only[i] = String.fromCharCode(n + init.charCodeAt());
           newLetter.push(letter_only[i])
            
          }
         
    }
  
    let capLetter = [];
    for(let i = 0; i < newLetter.length; i++){
      if(/[aeiou]/.test(newLetter[i])){
            capLetter.push(newLetter[i].toUpperCase());
          }else{
            capLetter.push(newLetter[i]);
          }
    }
  
    this.NewForm = capLetter.join("");
  
    // console.log(this.NewForm)
  
  }
}
